const Joi = require("joi");

const IntegranteEditSchema = Joi.object({
    rol: Joi.string().required().messages({
        'string.empty': 'El campo rol es obligatorio.',
        'any.required': 'El campo rol es obligatorio.'
    }),
    codigo: Joi.string().required().messages({
        'string.empty': 'El campo código es obligatorio.',
        'any.required': 'El campo código es obligatorio.'
    }),
    url: Joi.string().required().messages({
        //'string.uri': 'El campo URL debe ser una URL válida.',
        'string.empty': 'El campo URL es obligatorio.',
        'any.required': 'El campo URL es obligatorio.'
    }),
    esta_borrado: Joi.number().integer().min(0).max(1).required().messages({
        'number.base': 'El campo estado debe ser un número entero.',
        'number.min': 'El campo estado debe ser 0 (Activo) o 1 (Inactivo).',
        'number.max': 'El campo estado debe ser 0 (Activo) o 1 (Inactivo).',
        'any.required': 'El campo estado es obligatorio.'
    })
}).options({ allowUnknown: true });

module.exports = IntegranteEditSchema;
