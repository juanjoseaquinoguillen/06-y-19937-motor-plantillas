const Joi = require("joi");

const IntegranteStoreSchema = Joi.object({
    matricula: Joi.string().required().min(5).label("Matricula"),
    nombre: Joi.string().min(3).required().label("Nombre"),
    apellido: Joi.string().min(3).required().label("Apellido"),
    rol: Joi.string().valid('desarrollador', 'ayudante', 'analista').required().label("Rol"),
    codigo: Joi.string().required().label("Codigo"),
    url: Joi.string().optional().label("Url"),
    esta_borrado: Joi.required().label("Estado")
});

module.exports = IntegranteStoreSchema;


/*const Informacioncreacionejemplo ={
    nombre: "Juan",
}
const {error,value}= IntegranteStoreSchema.validate(
    Informacioncreacionejemplo
)
console.log("error",error)
console.log("value",value)
*/