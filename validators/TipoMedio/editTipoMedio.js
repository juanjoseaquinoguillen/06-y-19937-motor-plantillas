const Joi = require("joi");

const TipoMedioEditSchema = Joi.object({
    tipo: Joi.string().min(3).max(100).required().messages({
        'string.base': 'El tipo debe ser un texto.',
        'string.empty': 'El tipo no puede estar vacío.',
        'string.min': 'El tipo debe tener al menos 3 caracteres.',
        'string.max': 'El tipo no puede tener más de 100 caracteres.',
        'any.required': 'El tipo es un campo obligatorio.'
    }),
    descripcion: Joi.string().min(10).max(500).required().messages({
        'string.base': 'La descripción debe ser un texto.',
        'string.empty': 'La descripción no puede estar vacía.',
        'string.min': 'La descripción debe tener al menos 10 caracteres.',
        'string.max': 'La descripción no puede tener más de 500 caracteres.',
        'any.required': 'La descripción es un campo obligatorio.'
    }),
    esta_borrado: Joi.number().integer().min(0).max(1).required().messages({
        'number.base': 'El estado debe ser un número.',
        'number.integer': 'El estado debe ser un entero.',
        'number.min': 'El estado debe ser 0 (Activo) o 1 (Inactivo).',
        'number.max': 'El estado debe ser 0 (Activo) o 1 (Inactivo).',
        'any.required': 'El estado es un campo obligatorio.'
    })
}).options({ allowUnknown: true });

module.exports = TipoMedioEditSchema;
