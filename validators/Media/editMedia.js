const Joi = require("joi");

const MediaEditSchema = Joi.object({
    titulo: Joi.string().min(3).max(100).required().messages({
        'string.base': 'El título debe ser un texto.',
        'string.empty': 'El título no puede estar vacío.',
        'string.min': 'El título debe tener al menos 3 caracteres.',
        'string.max': 'El título no puede tener más de 100 caracteres.',
        'any.required': 'El título es un campo obligatorio.'
    }),
    parrafo: Joi.string().min(10).max(500).required().messages({
        'string.base': 'El párrafo debe ser un texto.',
        'string.empty': 'El párrafo no puede estar vacío.',
        'string.min': 'El párrafo debe tener al menos 10 caracteres.',
        'string.max': 'El párrafo no puede tener más de 500 caracteres.',
        'any.required': 'El párrafo es un campo obligatorio.'
    }),
    video: Joi.string().uri().optional().allow(null, '').messages({
        'string.uri': 'El enlace de video debe ser una URL válida.'
    }),
    esta_borrado: Joi.number().integer().min(0).max(1).required().messages({
        'number.base': 'El estado debe ser un número.',
        'number.integer': 'El estado debe ser un entero.',
        'number.min': 'El estado debe ser 0 (Activo) o 1 (Inactivo).',
        'number.max': 'El estado debe ser 0 (Activo) o 1 (Inactivo).',
        'any.required': 'El estado es un campo obligatorio.'
    })
}).options({ allowUnknown: true });

module.exports = MediaEditSchema;
