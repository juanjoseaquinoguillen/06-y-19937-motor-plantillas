const Joi = require("joi");

const MediaStoreSchema = Joi.object({
    url: Joi.string().required().messages({
        'any.required': 'La URL es obligatoria'
    }),
    titulo: Joi.string().required().messages({
        'any.required': 'El título es obligatorio'
    }),
    parrafo: Joi.string().required().messages({
        'any.required': 'El párrafo es obligatorio'
    }),
    video: Joi.string().optional(),
    esta_borrado: Joi.number().required().messages({
        'any.required': 'El estado es obligatorio'
    })
}).options({ allowUnknown: true }); // Permitir campos desconocidos en el objeto

module.exports = MediaStoreSchema;
