const express = require('express');
const { getALL, db } = require('../../db/conexion'); 
const TipoMedioStoreSchema = require('../../validators/TipoMedio/createTipoMedio');
const TipoMedioEditSchema = require('../../validators/TipoMedio/editTipoMedio')
const TipoMedioModel = require('../../models/TipoMedioModel')

const TipoMedioController = {
    index: async function (req, res) {
        try {
            // Extract filters from query parameters
            const filters = req.query.s || {};
            const validFilters = {};
            const validFields = ['id', 'tipo', 'descripcion', 'esta_borrado'];

            for (const key in filters) {
                if (validFields.includes(key)) {
                    validFilters[key] = filters[key];
                }
            }

            const tipoMedioItems = await TipoMedioModel.getALL(validFilters);

            const message = req.query.message || null;

            if (tipoMedioItems.length === 0) {
                return res.render("admin/TipoMedio/index", { message: "No hay elementos de tipo de medio", tipoMedioItems: [] });
            }

            res.render("admin/TipoMedio/index", { message, tipoMedioItems });
        } catch (error) {
            console.error(error);
            return res.status(500).render("error");
        }
    },

    create: async function (req, res) {
        const mensaje = req.query.mensaje;
        const datosForm = req.query;
        res.render('admin/TipoMedio/crearForm', {
            mensaje,
            datosForm,
        });
    },

    store: async function (req, res) {
        try {
            const { error, value } = TipoMedioStoreSchema.validate(req.body, { abortEarly: false });
            if (error) {
                const errorMessage = error.details.map(detail => detail.message).join(', ');
                const queryParams = new URLSearchParams(req.body).toString();
                return res.redirect(`/admin/TipoMedio/crear?mensaje=${encodeURIComponent(errorMessage)}&${queryParams}`);
            }

            const { tipo, descripcion, esta_borrado } = value;

            const tipoExistente = await TipoMedioModel.getByField('tipo', tipo);

            if (tipoExistente) {
                const queryParams = new URLSearchParams(req.body).toString();
                return res.redirect(`/admin/TipoMedio/crear?mensaje=El tipo ya existe&${queryParams}`);
            }

            await TipoMedioModel.create({ tipo, descripcion, esta_borrado });

            res.redirect("/admin/TipoMedio/listar?message=Tipo de Medio creado exitosamente");
        } catch (error) {
            console.error('Error al insertar en la base de datos:', error);
            res.status(500).send('Error al insertar en la base de datos');
        }
    },

    edit: async function (req, res) {
        const idTipoMedio = req.params.idTipoMedio;

        try {
            const tipoMedio = await TipoMedioModel.getById(idTipoMedio);

            if (!tipoMedio) {
                return res.redirect('/admin/TipoMedio/listar?message=No se encontró el tipo de medio');
            }

            res.render("admin/TipoMedio/editForm", { tipoMedio });
        } catch (error) {
            console.error("Error al obtener el tipo de medio:", error);
            return res.status(500).render("error");
        }
    },

    update: async function (req, res) {
        const idTipoMedio = req.params.idTipoMedio;

        try {
            const { error, value } = TipoMedioEditSchema.validate(req.body, { abortEarly: false });

            if (error) {
                const errorMessage = error.details.map(detail => detail.message).join(', ');
                return res.redirect(`/admin/TipoMedio/edit/${idTipoMedio}?mensaje=${encodeURIComponent(errorMessage)}`);
            }

            await TipoMedioModel.update(idTipoMedio, value);

            res.redirect('/admin/TipoMedio/listar?message=Tipo de medio actualizado exitosamente');
        } catch (error) {
            console.error("Error al actualizar el tipo de medio:", error);
            res.redirect(`/admin/TipoMedio/edit/${idTipoMedio}?mensaje=Error al actualizar el tipo de medio`);
        }
    },

    destroy: async function (req, res) {
        const idTipoMedio = req.params.idTipoMedio;

        try {
            await TipoMedioModel.delete(idTipoMedio);

            res.redirect('/admin/TipoMedio/listar?message=Tipo de medio eliminado exitosamente');
        } catch (error) {
            console.error("Error al eliminar el tipo de medio:", error);
            res.redirect('/admin/TipoMedio/listar?message=Error al eliminar el tipo de medio');
        }
    }
};

module.exports = TipoMedioController;
