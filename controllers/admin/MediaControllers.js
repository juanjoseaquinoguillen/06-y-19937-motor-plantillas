const express = require('express');
const { getALL, db } = require('../../db/conexion');
const path = require('path');
const fs = require('fs');
const MediaStoreSchema = require('../../validators/Media/createMedia');
const MediaEditSchema = require('../../validators/Media/editMedia');
const MediaModel = require('../../models/MediaModel');

function convertToEmbedURL(url) {
    console.log("youtube:", url);
    if (!url) {
        return null;
    }

    const regex = /(?:https?:\/\/)?(?:www\.)?(?:youtube\.com\/(?:[^\/\n\s]+\/\S+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([a-zA-Z0-9_-]{11})/;
    const match = url.match(regex);
    if (match && match[1]) {
        return `https://www.youtube.com/embed/${match[1]}`;
    }
    return null;
}

const MediaController = {
    index: async function (req, res) {
        try {
            const filters = req.query.s || {};
            const mediaItems = await MediaModel.getALL(filters);

            const message = req.query.message || null;

            if (mediaItems.length === 0) {
                return res.render("admin/Media/index", { message: "No hay elementos de media", mediaItems: [] });
            }

            res.render("admin/Media/index", { mediaItems, message });
        } catch (error) {
            console.error(error);
            return res.status(500).render("error");
        }
    },
    create: async function (req, res) {
        const mensaje = req.query.message;
        try {
            const integrantesActivos = await getALL(`SELECT * FROM Integrante WHERE esta_borrado = 0`);
            const tiposMedia = await getALL(`SELECT * FROM TipoMedio`);
            const datosForm = req.query || {};

            res.render('admin/Media/crearForm', {
                mensaje,
                datosForm,
                integrantesActivos,
                tiposMedia,
            });
        } catch (error) {
            console.error('Error al obtener datos de la base de datos:', error);
            res.status(500).send('Error al obtener datos de la base de datos');
        }
    },

    store: async function (req, res) {
        try {
            const { error, value } = MediaStoreSchema.validate(req.body, { abortEarly: false });
            if (error) {
                const errorMessage = error.details.map(detail => detail.message).join(', ');
                const queryString = Object.entries(req.body).map(([key, value]) => `${key}=${encodeURIComponent(value)}`).join('&');
                return res.redirect(`/admin/Media/crear?message=${encodeURIComponent(errorMessage)}&${queryString}`);
            }

            const { matricula, url, titulo, parrafo, video, tipo_medio, esta_borrado } = value;

            let imagen = null;
            if (req.file) {
                imagen = `/assets/${req.file.filename}`;
            }

            const embedVideoURL = convertToEmbedURL(video);

            const maxOrdenResult = await new Promise((resolve, reject) => {
                db.get("SELECT MAX(orden) as maxOrden FROM Media", (err, row) => {
                    if (err) {
                        return reject(err);
                    }
                    resolve(row);
                });
            });

            const newOrden = (maxOrdenResult.maxOrden || 0) + 1;

            db.run(
                "INSERT INTO Media (matricula, url, titulo, parrafo, imagen, video, tipo_medio, esta_borrado, orden) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                [matricula, url, titulo, parrafo, imagen, embedVideoURL, tipo_medio, esta_borrado, newOrden],
                (err) => {
                    if (err) {
                        console.error("Error al insertar en la base de datos:", err);
                        return res.redirect('/admin/Media/crear?message=Error al insertar en la base de datos');
                    }
                    res.redirect("/admin/Media/listar?message=Elemento de media creado exitosamente");
                }
            );
        } catch (error) {
            console.error('Error al insertar en la base de datos:', error);
            res.redirect('/admin/Media/crear?message=Error al insertar en la base de datos');
        }
    },

    edit: async function (req, res) {
        try{
        const idMedia = req.params.idMedia;
        console.log("ID del Media:", idMedia

        );
        const Media = await MediaModel.getById(idMedia);

            if (!Media) {
                return res.redirect('/admin/Media/listar?message=No se encontró la Media');
            }

            res.render("admin/Media/editForm", { Media });
        } catch (error) {
            console.error("Error al obtener la Media:", error);
            return res.status(500).render("error");
        }

        /**try {
            const media = await new Promise((resolve, reject) => {
                db.get("SELECT m.*, i.nombre, i.apellido FROM Media m INNER JOIN Integrante i ON m.matricula = i.matricula WHERE m.id = ?", [idMedia], (err, row) => {
                    if (err) {
                        return reject(err);
                    }
                    resolve(row);
                });
            });

            if (!media) {
                return res.redirect('/admin/Media/listar?message=No se encontró el medio');
            }

            res.render("admin/Media/editForm", { media });
        } catch (error) {
            console.error("Error al obtener el medio:", error);
            res.status(500).render("error");
        }**/
    },

    update: async function (req, res) {
        const idMedia = req.params.idMedia;

        try {
            const { error, value } = MediaEditSchema.validate(req.body, { abortEarly: false });

            if (error) {
                const errorMessage = error.details.map(detail => detail.message).join(', ');
                console.error("Error de validación:", errorMessage);
                return res.redirect(`/admin/Media/edit/${idMedia}?message=${encodeURIComponent(errorMessage)}`);
            }

            const { titulo, parrafo, video, esta_borrado } = value;
            let imagen = req.body.imagen;

            if (req.file) {
                imagen = `/assets/${req.file.filename}`;
            }

            let embedVideoURL = null;
            if (video) {
                embedVideoURL = convertToEmbedURL(video);
            }

            const sql = `
                UPDATE Media
                SET titulo = ?, parrafo = ?, imagen = ?, video = ?, esta_borrado = ?
                WHERE id = ?
            `;
            const params = [titulo, parrafo, imagen, embedVideoURL, esta_borrado, idMedia];

            await new Promise((resolve, reject) => {
                db.run(sql, params, function (err) {
                    if (err) {
                        return reject(err);
                    }
                    resolve();
                });
            });

            console.log(`Medio con ID ${idMedia} actualizado.`);
            res.redirect('/admin/Media/listar?message=Medio actualizado exitosamente');
        } catch (error) {
            console.error("Error al actualizar el medio:", error);
            res.redirect(`/admin/Media/edit/${idMedia}?message=Error al actualizar el medio`);
        }
    },

    destroy: async function (req, res) {
        const idMedia = req.params.idMedia;
        try {
            await new Promise((resolve, reject) => {
                db.run("UPDATE Media SET esta_borrado = 1 WHERE id = ?", [idMedia], function (err) {
                    if (err) {
                        return reject(err);
                    }
                    resolve();
                });
            });

            console.log(`Medio con ID ${idMedia} marcado como inactivo.`);
            res.redirect('/admin/Media/listar?message=Medio eliminado exitosamente');
        } catch (error) {
            console.error("Error al marcar el medio como inactivo:", error);
            res.redirect('/admin/Media/listar?message=Error al eliminar el medio');
        }
    }
};

module.exports = MediaController;
