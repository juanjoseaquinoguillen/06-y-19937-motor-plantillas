/*
getAll - obtener todos los registros-filtros opcionales de busqueda deben estar disponible por cada campo de la tabla
getById- Obtener un registro por ID
getByField - Obtener un registro por campo arbitrario
create - crear registro
update - actualizar registro
delete - borrar registro
*/ 

const { getALL, db } = require('../db/conexion'); 

const IntegranteModel = {
    getALL: async function (filters = {}) {
        try {
            let sql = "SELECT * FROM Integrante WHERE esta_borrado = 0";
            for (const prop in filters) {
                if (filters[prop]) {
                    sql += ` AND ${prop} LIKE '%${filters[prop]}%'`;
                }
            }

            let integrantes = await getALL(sql);

            if (integrantes.length === 0) {
                return [];
            }

            const processedIntegrantes = await Promise.all(integrantes.map(async integrante => {
                const datos = await getALL(`SELECT * FROM Media WHERE matricula = '${integrante.matricula}'`);
                return {
                    ...integrante,
                    esta_borrado: integrante.esta_borrado ? 'Inactivo' : 'Activo',
                    datos
                };
            }));

            return processedIntegrantes;
        } catch (error) {
            console.error(error);
            throw new Error("Error al obtener los integrantes");
        }
    },
    getById: async function (id) {
        try {
            const integrante = await new Promise((resolve, reject) => {
                db.get("SELECT * FROM Integrante WHERE id = ?", [id], (err, row) => {
                    if (err) {
                        return reject(err);
                    }
                    resolve(row);
                });
            });

            if (!integrante || integrante.esta_borrado) {
                return null;
            }

            return integrante;
        } catch (error) {
            console.error(error);
            throw new Error("Error al obtener el integrante por ID");
        }
    },
    getByField: async function (field, value) {
        try {
            const integrantes = await new Promise((resolve, reject) => {
                db.all(`SELECT * FROM Integrante WHERE ${field} = ? AND esta_borrado = 0`, [value], (err, rows) => {
                    if (err) {
                        return reject(err);
                    }
                    resolve(rows);
                });
            });

            return integrantes;
        } catch (error) {
            console.error(error);
            throw new Error(`Error al obtener integrantes por ${field}`);
        }
    },
    create: async function (integranteData) {
        try {
            const { matricula, nombre, apellido, rol, codigo, url, esta_borrado } = integranteData;
            
            const maxOrdenResult = await new Promise((resolve, reject) => {
                db.get("SELECT MAX(orden) as maxOrden FROM Integrante", (err, row) => {
                    if (err) {
                        return reject(err);
                    }
                    resolve(row);
                });
            });

            const newOrden = (maxOrdenResult.maxOrden || 0) + 1;

            await new Promise((resolve, reject) => {
                db.run(
                    "INSERT INTO Integrante (matricula, nombre, apellido, rol, codigo, url, esta_borrado, orden) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
                    [matricula, nombre, apellido, rol, codigo, url, esta_borrado, newOrden],
                    (err) => {
                        if (err) {
                            return reject(err);
                        }
                        resolve();
                    }
                );
            });

            return { message: "Integrante creado exitosamente" };
        } catch (error) {
            console.error(error);
            throw new Error("Error al crear el integrante");
        }
    },
    update: async function (id, integranteData) {
        try {
            const { rol, codigo, url, esta_borrado } = integranteData;

            await new Promise((resolve, reject) => {
                db.run(
                    "UPDATE Integrante SET rol = ?, codigo = ?, url = ?, esta_borrado = ? WHERE id = ?",
                    [rol, codigo, url, esta_borrado, id],
                    (err) => {
                        if (err) {
                            return reject(err);
                        }
                        resolve();
                    }
                );
            });

            return { message: "Integrante actualizado exitosamente" };
        } catch (error) {
            console.error(error);
            throw new Error("Error al actualizar el integrante");
        }
    },
    delete: async function (id) {
        try {
            await new Promise((resolve, reject) => {
                db.run("UPDATE Integrante SET esta_borrado = 1 WHERE id = ?", [id], (err) => {
                    if (err) {
                        return reject(err);
                    }
                    resolve();
                });
            });

            return { message: "Integrante eliminado exitosamente" };
        } catch (error) {
            console.error(error);
            throw new Error("Error al eliminar el integrante");
        }
    }
};

module.exports = IntegranteModel;
