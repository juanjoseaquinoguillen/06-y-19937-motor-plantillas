const express = require('express');
const router = express.Router();
const hbs = require('hbs');
const multer = require('multer');
const path = require('path');

// Configuración de multer para guardar las imágenes en la carpeta correcta
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.join(__dirname, '..', '..', 'public', 'assets'));
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + path.extname(file.originalname));
    }
});
const upload = multer({ storage: storage });
const fileUpload = upload.single('imagen');

// Controladores
const IntegranteController = require('../controllers/admin/IntegranteControllers');
const TipoMedioController = require('../controllers/admin/TipoMedioControllers');
const MediaController = require('../controllers/admin/MediaControllers');

hbs.registerHelper('eq', function (a, b) {
    return a == b;
});


// Middleware de autenticación
const authMiddleware = (req, res, next) => {
    if (req.session.logueado) {
        // Si el usuario está autenticado, permitir que continúe
        next();
    } else {
        // Si el usuario no está autenticado, redirigirlo al inicio de sesión
        res.redirect('/auth/login');
    }
};
// Rutas
router.get('/', (req, res) => {
    if(!req.session.logueado)return res.redirect("auth/login")
    res.render('admin/index');
});

router.get('/Integrante/listar',authMiddleware, IntegranteController.index);
router.get('/Integrante/crear',authMiddleware, IntegranteController.create);
router.post('/Integrante/create',authMiddleware, IntegranteController.store);
router.post('/Integrante/delete/:idIntegrante',authMiddleware, IntegranteController.destroy);
router.get('/Integrante/edit/:idIntegrante',authMiddleware, IntegranteController.edit);
router.post('/Integrante/update/:idIntegrante',authMiddleware, IntegranteController.update);

router.get('/TipoMedio/listar',authMiddleware, TipoMedioController.index);
router.get('/TipoMedio/crear',authMiddleware, TipoMedioController.create);
router.post('/TipoMedio/create',authMiddleware, TipoMedioController.store);
router.post('/TipoMedio/delete/:idTipoMedio',authMiddleware, TipoMedioController.destroy);
router.get('/TipoMedio/edit/:idTipoMedio',authMiddleware, TipoMedioController.edit);
router.post('/TipoMedio/update/:idTipoMedio',authMiddleware, TipoMedioController.update);

router.get('/Media/listar',authMiddleware, MediaController.index);
router.get('/Media/crear',authMiddleware, MediaController.create);
router.post('/Media/create',authMiddleware, fileUpload, MediaController.store);
router.post('/Media/delete/:idMedia',authMiddleware, MediaController.destroy);
router.get('/Media/edit/:idMedia',authMiddleware, MediaController.edit);
router.post('/Media/update/:idMedia',authMiddleware, fileUpload, MediaController.update); // Asegúrate de usar fileUpload aquí también


module.exports = router;
