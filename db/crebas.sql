

CREATE TABLE "Integrante" (
	"id"	INTEGER NOT NULL,
	"matricula"	VARCHAR(255) NOT NULL,
	"nombre"	VARCHAR(255) NOT NULL,
	"apellido"	VARCHAR(255) NOT NULL,
	"rol"	VARCHAR(255) NOT NULL,
	"codigo"	VARCHAR(255) NOT NULL,
	"url"	VARCHAR(255) NOT NULL,
	"esta_borrado"	BOOLEAN DEFAULT FALSE,
	"orden"	INTEGER DEFAULT 0,
	PRIMARY KEY("id" AUTOINCREMENT)
);

"id"	INTEGER,
	"matricula"	VARCHAR(255) NOT NULL,

CREATE TABLE IF NOT EXISTS Media (
  id INTEGER PRIMARY KEY, -- Agregamos una nueva columna para ser la clave primaria
  matricula VARCHAR(255) NOT NULL, -- Quitamos la restricción de clave primaria de esta columna
  url VARCHAR(255) NOT NULL,
  titulo VARCHAR(255) NOT NULL,
  parrafo TEXT NOT NULL,
  imagen TEXT,
  video TEXT,
  html TEXT,
  tipo_medio VARCHAR(255) NOT NULL,
  esta_borrado BOOLEAN DEFAULT FALSE,
  FOREIGN KEY (matricula) REFERENCES Integrante(matricula), -- Agregamos una restricción de clave externa
  FOREIGN KEY (tipo_medio) REFERENCES TipoMedio(tipo)
);

CREATE TABLE IF NOT EXISTS TipoMedio (
  tipo VARCHAR(255) PRIMARY KEY,  
  descripcion TEXT NOT NULL,
  esta_borrado BOOLEAN DEFAULT FALSE
);

CREATE INDEX IF NOT EXISTS idx_media_tipo ON Media(tipo_medio);
CREATE INDEX IF NOT EXISTS idx_media_integrante ON Media(matricula);


ALTER TABLE Integrante
ADD COLUMN orden INTEGER NOT NULL DEFAULT 0;

ALTER TABLE Media
ADD COLUMN orden INTEGER NOT NULL DEFAULT 0;

ALTER TABLE TipoMedio
ADD COLUMN orden INTEGER NOT NULL DEFAULT 0;

CREATE TABLE "usuarios" (
	"id"	INTEGER,
	"usuario"	TEXT NOT NULL UNIQUE,
	"password"	TEXT NOT NULL,
	"sysadmin"	BOOLEAN DEFAULT 0,
	"admin"	BOOLEAN DEFAULT 0,
	"created_at"	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY("id" AUTOINCREMENT)
);