
INSERT INTO Integrante (matricula, nombre, apellido, rol, codigo, url, esta_borrado)
VALUES ('Y25387', 'Junior', 'Cabral', 'Desarrollador', '02', 'Y25387/index.html', FALSE);

INSERT INTO Integrante (matricula, nombre, apellido, rol, codigo, url, esta_borrado)
VALUES ('Y25495', 'Sebastian', 'Duarte', 'Desarrollador', '03', 'Y25495/index.html', FALSE);

INSERT INTO Integrante (matricula, nombre, apellido, rol, codigo, url, esta_borrado)
VALUES ('Y19099', 'Elvio', 'Aguero', 'Desarrollador', '04', 'Y19099/index.html', FALSE);

INSERT INTO Integrante (matricula, nombre, apellido, rol, codigo, url, esta_borrado)
VALUES ('UG0085', 'Luis', 'Delgado', 'Desarrollador', '05', 'UG0085/index.html', FALSE);


INSERT OR IGNORE INTO TipoMedio (tipo, descripcion, esta_borrado)
VALUES
  ('Youtube', 'Videos en linea', FALSE),
  ('Twitter/X', 'Mensajes cortos y multimedia', FALSE),
  ('Dibujo', 'Ilustraciones y gráficos', FALSE);


-- Insertar datos para Junior
INSERT INTO Media (matricula, url, titulo, parrafo, imagen, video, html, tipo_medio, esta_borrado)
VALUES
  ('Y25387', 'Y25387/index.html', 'Mi Video Favorito en YouTube.', 'Uno de los vídeos más interesantes para mí trata sobre la idea preconcebida de que estamos hechos de partículas, sino de algo más, tal vez campos que vibran en el espacio y en base a esas fluctuaciones existimos.', '', 'https://www.youtube.com/embed/Tsnyq-3k7Bg?si=-Bzir8axv4WRU4MS&amp;controls=0', '', 'Youtube', FALSE),
  ('Y25387', 'Y25387/index.html', 'Imagen Favorita', 'Una de las cosas que más me gusta hacer es observar el atardecer. Es un momento de paz y tranquilidad que me ayuda a relajarme y pensar en las cosas que me importan, nunca dejando de cuestionarme los hechos físicos que llevan a que se puedan observar, como en esta imagen', '../../assets/Junior%20-%20Foto.jpg', '', '', 'Imagen', FALSE),
  ('Y25387', 'Y25387/index.html', 'Dibujo Propio', 'Algo que me apasiona bastante, además de la física de partículas y los atardeceres, es poder mirar más allá de donde estamos, imaginar los mundos posibles en la vastedad del cosmos y lo impresionante que son, como este dibujo de un planeta con un sistema de anillos parecido al de Júpiter.', '../../assets/Junior-Dibujo.png', '', '', 'Dibujo', FALSE);

-- Insertar datos para Sebastian
INSERT INTO Media (matricula, url, titulo, parrafo, imagen, video, html, tipo_medio, esta_borrado)
VALUES
  ('Y25495', 'Y25495/index.html', 'Mi Video Favorito en YouTube.', 'Mi video favorito es el trailer de la última temporada de mi serie favorita, considerada una obra maestra narrativa y visual. Para mí, simboliza el fin de una etapa en mi vida, ya que seguí esta serie desde hace 8 años.', '', 'https://www.youtube.com/embed/FRn6xXXF-7s?si=yVJP3egE0MB4siuE', '', 'Youtube', FALSE),
  ('Y25495', 'Y25495/index.html', 'Imagen Favorita', 'Mi imagen favorita es el protagonista del videojuego Dark Souls, y la imagen representa para mí su desesperanza y su impotencia, pero al mismo tiempo el objetivo que lo mantiene en su travesía. Me gusta por todo lo que representa para mí y todo lo significó para mí el juego y su historia bien elaborada.', '../../assets/Sebastian-foto.webp', '', '', 'Imagen', FALSE),
  ('Y25495', 'Y25495/index.html', 'Dibujo Propio', 'Mi dibujo siento que representa la perseverancia, ya que esta fogata es aquella en la que los aventureros descansan y se arman de valor para seguir con su viaje. También está sacada del videojuego Dark Souls.', '../../assets/Sebastian-Dibujo.jpg', '', '', 'Dibujo', FALSE);


-- Insertar datos para Elvio
INSERT INTO Media (matricula, url, titulo, parrafo, imagen, video, html, tipo_medio, esta_borrado)
VALUES
  ('Y19099', '', 'Mi Video Favorito en YouTube.', '', '', 'https://www.youtube.com/embed/NynNdkY2gx0?si=nzQjEF5y6hY6uSOp', '', 'Youtube', FALSE),
  ('Y19099', 'Y19099/index.html', 'Imagen Favorita', '', '../../assets/Elvio-Foto.jpg', '', '', 'Imagen', FALSE),
  ('Y19099', 'Y19099/index.html', 'Dibujo Propio', '', '../../assets/Elvio-Dibujo.png', '', '', 'Dibujo', FALSE);

-- Insertar datos para Luis
INSERT INTO Media (matricula, url, titulo, parrafo, imagen, video, html, tipo_medio, esta_borrado)
VALUES
  ('UG0085', 'UG0085/index.html', 'Mi Video Favorito en YouTube.', 'Uno de los vídeos que más me llama la atención es acerca de un libro que se llama La Vida Secreta De La Mente.', '', 'https://www.youtube.com/embed/Hv5ET7azgEk?si=WxryTH3oIMGTTD-Z', '', 'Youtube', FALSE),
  ('UG0085', 'UG0085/index.html', 'Imagen Favorita', 'Una de las cosas que más me gusta es escalar cerros y observar los hermosos paisajes de nuestro país. Cerro Hu', '../../assets/Luis Delgado.jpeg', '', '', 'Imagen', FALSE),
  ('UG0085', 'UG0085/index.html', 'Dibujo Propio', 'Siempre es importante aprender algo nuevo.', '../../assets/Luis Delgado-2.png', '', '', 'Dibujo', FALSE);

-- Insertar datos para el Home
INSERT INTO Media (matricula, url, titulo, parrafo, video, html, tipo_medio, esta_borrado)
VALUES ('Home', '/', 'Home', 'Pagina Grupo 01', 'Logo Del grupo', './assets/logo.jpeg', 'tipo_de_medio', FALSE);


-- Insertar usuario admin
INSERT INTO usuarios (usuario, password, sysadmin, admin) VALUES ('juanjoseaquinoguillen@gmail.com', '$2b$10$wHcBfA1fZ1R2JstltQj.DOE0GpNZ2u/FcxWY8dF.4iKZLgY4.NBva', 0, 1);

-- Insertar usuario juan
INSERT INTO usuarios (usuario, password, sysadmin, admin) VALUES ('juan02mejor@gmail.com', '$2b$10$N9qo8uLOickgx2ZMRZo5i.U5vG7xoxJcTofHcGqXfNSB2yEvlCoOm', 0, 0);

